# OAP9 Changelog (2022-04-29)
Based on autoware.ai version 1.14.0

## Common
- Reduce build warnings (eee7f52)
- Add Lanelet2 ENU Projector (539a9e5)
- Change dependency from python-numpy to python3-numpy (6226c47)
- Port repo to Noetic (1b488ac)
- CI: Update to Noetic (162bb93)
- Update to python3 (147c56b)

## Core Perception
- ENU frame script (184270d)
- Reduce build warnings (614d155)
- Port repo to Noetic (f5fa857)
- CI: Update to Noetic (3a5366c)
- Fix waypoint gid generation for waypoints with invalid gid of 0 (4607289)
- gpsins_localizer: Fix typo in README (9849a42)
- Remove dependency on novatel_gps_msgs (578a96d)

## Core Planning
- twist_gate: Don't send gear cmd when using LGSVL sim (678a6ed)
- Update gear value in LGSVL sim (a3c4849)
- Reduce build warnings (0209211)
- Port repo to Noetic (8462281)
- CI: Update to Noetic (6900ab6)
- Load lidar frame id from config ros params instead hard coded one (eb2f4ff)

## Docker
- Noetic cuda dockerfile (30cdbf8)
- Add noninteractive arg to dockerfile (7a03a52)
- Noetic support (46db2b4)
- Add hadolint config file (b3d5243)

## Drivers
- Reduce build warnings (e74307b)
- Port repo to Noetic (490fa87)
- CI: Update to Noetic (ab7087a)

## Messages
- Remove ROS version from CI job name (c08bff6)
- Reduce build warnings (f2ae193)
- CI: Update to Noetic (19eff8b)

## Simulation
- Remove unused vehicle_gazebo packages (a87be84)
- Reduce build warnings (2e5f949)
- Port repo to Noetic (b3bfc99)
- CI: Update to Noetic (7837a47)
- Update to Python3 (2d72019)
- Remove python-yaml dependency (8716a6d)

## Utilities
- calibration_publisher: Don't publish undistorted image by default (1e27d8b)
- Add undistorted image publisher to calibration_publisher (43fccf9)
- Extrinsic calibration dynamic reconfigure utility (a9350ef)
- Reduce build warnings (65ba90c)
- Port repo to Noetic (b10b9b8)
- CI: Update to Noetic (06c82f0)
- Update to Python3 (555c535)
- Remove uvc_camera dependency from runtime_manager (b3d5bae)

## Visualization
- Reduce build warnings (1ff7b8c)
- Port repo to Noetic (9f1bc5d)
- CI: Update to Noetic (d459aa8)

---

# OAP8 Changelog (2021-06-11)
Based on autoware.ai version 1.14.0

## Common
- Improve amathutils::isIntersectLine (772fa00)
- Add highway lanelets to visualization (24c9c75)

## Core Perception
- Update Apollo CNN (f45f8f4)
- lidar_apollo_cnn_seg_detect: Add blank default args (c8ff597)
- naive_motion_predict: standardize topic, launch files, ns (91aba4c)
- Standardize topics and launch files for apollo_cnn_seg_detect and naive_l_shape_detect (9be1120)
- lidar_shape_estimation: Add generic launch file (c0784c7)
- imm_ukf_pda_track: Update topic names, clarify callback (68771ca)
- Lidar_euclidean_cluster_detect : code tidy, added tfs for convex_hull + BoundingBox (28c2ebd)
- Improve trafficlight recognizer ROI extractor (ba9d1e7)

## Core Planning
- decision_maker: Add explicit cast for stopline id and change detection objects subscribing topic (434951c)
- Make decision_maker single-threaded (5a19bfc)
- stanley_controller: Set default output_interface to ctrl_cmd (1054fa9)
- waypoint_replanner: Only override waypoint velocities with new velocity_override param (fbe4bbe)
- Ability to save gps position with waypoints (8f657e0)
- waypoint_marker_publisher: Add subscriber option for Lane instead of LaneArray, add update frequency limiting (13c38ea)
- Fix decision maker unit test (f63bdd7)
- decision_maker: fix multiple stops (2c632ea)
- decision_maker: Add distance condition to detect ego vehicle's stop (4f9a250)
- decision_maker: Fix convhull infinite loop (ebc32a1)
- astar_avoid: Re-add missing cleanups (6f37e82)

## Docker
- Fix eigen install (c700561)
- Update eigen download url (e7b49ee)

## Drivers
- ssc_interface: Change max_curvature_rate to something realistic (020aba9)

## Messages

## Simulation

## Utilities
- cam_lidar_calibration: param update (958e992)
- cam_lidar_calibration: add generated msgs to add_dependencies (d189581)
- Add new cam_lidar_calibration pkg (9a77f08)

## Visualization

---

# OAP7 Changelog (2020-10-02)
Based on autoware.ai version 1.14.0

## Common
- Remove logging from library functions (00d9a46)
- Upgrade Lanelet2 version (b719ed6)
- Use rosout in points_map_loader (96e1447)
- Fix libwaypoint_follower return type bug (512f3d3)
- Update to Autoware.ai version 1.14.0 (ff46431)
- CI: Remove ARM cross-compilation (2067313)
- Fix third-party build dependencies (8f731aa)

## Core Perception
- ndt_matching: Remove leading slash from all map frames (3aed2f2)
- Add tensorflow_tlr back to CMakeLists, mark script executable (0e29b54)
- Add build warning for ndt_cpu (5d29805)
- Remove old/unused packages (be683cb)
- ekf_localizer: Add parameter for tf frame name (f78357f)
- Clean up lidar_imm_ukf_pda_track logging (c40505f)
- Clean up lidar_apollo_cnn_seg_detect logging (339afac)
- Upgrade Lanelet2 version (f232036)
- Update to Autoware.ai version 1.14.0 (4f748bd)
- Support for new Novatel driver "novatel_oem7_driver" (0fddf5f)
- CI: Remove ARM cross-compilation (d37c51d)
- Fix third-party build dependencies (4a35fde)
- Fix ray_ground_filter Namespacing (03eab0b)
- Individual launch files for can_status_translater and localization remapping (975c446)

## Core Planning
- Expand stop area in both direction of stop line (cccbf2c)
- 4-way stop iteration 2, proceeds when ego's turn (e0936f9)
- Fix stop signs on loop mode (b7f8b3a)
- decision_maker: Only abort mission when far from waypoints while also in autonomy_mode (f3ede65)
- lane_select: Fix distance_threshold bug (49c6b02)
- Twist Filter: Add Debug and Smoothing params (8988094)
- lane_select: Configurable params for lookahead & add distance check for lookahead distance (e1a5f98)
- Pure Pursuit Velocity Cmd Refactor (af0584c)
- Twist Gate Refactor (3f9e082)
- Velocity Set Acceleration Refactor (40dd5a7)
- waypoint loop mode (7dccfb7)
- stanley_controller: Fix preview_window param typo (48a8bda)
- Planning Logging Refactor (db5fd97)
- stanley_controller: Add lateral tracking error publisher (8306030)
- pure_pursuit: Improve ROS logging (e7fe012)
- astar_void: Fix crashing bug and further cleanup. (d5108fb)
- Lane change turn signals (14224e8)
- decision_maker: Remove duplicated code to check validity of a mission (c206881)
- decision_maker: Parameter name cleanup (73e2041)
- Add 4-way stop support (c26e1ec)
- Remove ros parameter stop_sign_id since it is no longer needed. (c51d00a)
- decision_maker: Fix incorrect start index inside waypoint iteration loop to compute distance (8ace344)
- Plan routes to LLH goals (fcd2989)
- Upgrade Lanelet2 version (29fd13a)
- astar_avoid: Handle failed search of the closest waypoint (001b333)
- Lanelet2 Global Planner Implementation (74a588a)
- decision_maker: Add comments for AutowareStatus member variables (45f44fa)
- Remove meaningless enum definition of TrafficLightColors in state machine lib. (84a4c9a)
- decision_maker: Change interal velocity units from km/h to m/s (43bc071)
- Engage decision_maker with ssc_interface (fa04e73)
- velocity_set: Improve stopping at stop signs (e443226)
- Update to Autoware.ai version 1.14.0 (b12244a)
- Lanelet2 Global Planner Skeleton (fcbeaa0)
- Clean up of lane_select node (197808d)
- CI: Remove ARM cross-compilation (c579aae)
- pure pursuit cleanup (de089b1)
- AS license info for stanley controller (7e19387)
- Fix third-party build dependencies (faaf51b)
- waypoint_replanner: Configuration parameter cleanup (4e60ba2)
- waypoint_replanner: Fix limitAccelDecel issue when the velocity profile contains multiple up and downs (93b4e37)
- waypoint_saver: Add ability to specify the location of waypoint to be saved (5695b8e)

## Docker

## Drivers
- Remove drivers that are either duplicates from elsewhere or are old and no longer used/maintained (e1ea9cd)
- Upgrade Lanelet2 version (fe50f17)
- Fix ssc_interface timeout and invalid check (c1c46ed)
- Update to Autoware.ai version 1.14.0 (05064ce)
- CI: Remove ARM cross-compilation (772a1a2)
- Fix third-party build dependencies (c794429)

## Messages
- Update to Autoware.ai version 1.14.0 (0504c31)
- CI: Remove ARM cross-compilation (5f127d0)
- waypoint_replanner configuration parameter cleanup (7931013)

## Simulation
- Upgrade Lanelet2 version (1e5f1bd)
- Update to Autoware.ai version 1.14.0 (d225947)
- CI: Remove ARM cross-compilation (0389745)
- Fix third-party build dependencies (45dd707)

## Utilities
- Remove unused image_rectifier from calibration launch (0ddd613)
- Remove old/unused packages (e63c7c5)
- Upgrade Lanelet2 version (249ec99)
- Update to Autoware.ai version 1.14.0 (baf0e64)
- CI: Remove ARM cross-compilation (3b5dcec)
- Fix third-party build dependencies (2fd3089)
- Improve multi-lidar calibration parameters (90badf3)
- Update autoware_launcher for waypoint_replanner and runtime manager cleanup. (2989b67)

## Visualization
- Remove old/unused packages (5deb37f)
- Upgrade Lanelet2 version (dd2afe0)
- Update to Autoware.ai version 1.14.0 (6ba6cc5)
- CI: Remove ARM cross-compilation (2ff3222)
- Fix third-party build dependencies (a020711)

---

# OAP6 Changelog (2020-05-01)
Based on autoware.ai version 1.13.0

## Common
- Fix Lanelet Utilities Bug (a2f4454)
- Add roslint to various packages (d392ba8)
- Add AllWayStop Support to lanelet2_extension (c8d63e8)
- CI: Adding --verbose flag to colcon test-result. (3ff9ce2)

## Core Perception
- Fix lidar_fake_perception pointcloud output frame (1f6417c)
- Fix vectormap_frame launch param (a742280)
- Migrate ndt_matching to tf2 (a041770)
- Add parameter to disable gpsins_localizer TF output (0fac1ae)
- Fix feat_proj params (b16f7d4)
- Query transform in between baselink and primary lidar through tf tree (4d08f1e)
- Cleanup lidar localizer ros params for lidar transformation (2d51b09)
- Fix feature projection launch file remap, output to screen (baca9f2)
- Add subscritpion to vehicle/twist directly inside lidar localizer. (f1d372c)
- CI: Adding --verbose flag to colcon test-result. (9fccf6b)
- Mean Sea Level Height option for gpsins_localizer (3c26865)

## Core Planning
- Fix vector map lane change signalling (565f2f9)
- Rename disuse_vector_map param, fix implementation of the parameter (92e8dff)
- Change log level from WARN to DEBUG for some lane_select msgs (8545563)
- Fix lane_select to differentiate stopline types (f3c7220)
- Add yellow arrows to the global path to indicate turn signal activation (c7f63a1)
- Removed velocity_set dependance on localizer_pose (c5ab8ae)
- Remove unnecessary check on stoplines from decision_maker (3335f46)
- Clean up Decision Maker (21ee00e)
- Remove qpoases_vendor from build_depends.repos. (cfdd5a9)
- Lint Decision Maker (22867c6)
- Clean up how control cmds are published (1448022)
- Fix velocity_set Namespace (304d2df)
- Fix lane_rule namespace (4a556e3)
- Enable roslint for all files (4f824bf)
- Revert pure_pursuit command velocity fix (4505916)
- Update lanelet function names (3657477)
- Fix the problem fo not publishing /base_waypoints (17fbbd5)
- CI: Adding --verbose flag to colcon test-result. (89747d1)
- Fix an inaccurate warning msg inside VelocitySetPath::calcInterval (4672d41)
- Pure pursuit: add update_rate as a configurable parameter (be12a9a)
- Stanley_controller: rename some parameters to be consistent with other packages (f965982)

## Docker

## Drivers
- Disengage SSC if control commands stop (cde1e5b)
- Make ssc_interface asynchronous, and re-enable fix (dd7bb8c)
- Delete YMC interface (d49510d)
- CI: Adding --verbose flag to colcon test-result. (928569e)
- Pointer member variables are not deleted inside destructor (37e20f9)

## Messages
- CI: Adding --verbose flag to colcon test-result. (8313dd4)

## Simulation
- CI: Adding --verbose flag to colcon test-result. (456c1d2)

## Utilities
- Use rqt_image_view for calibratoin by default (9254b6d)
- Improve Autoware Calibration Publisher (3390f1a)
- Add support for rqt_image_view to calibration process (c10b5f5)
- CI: Adding --verbose flag to colcon test-result. (5f375ad)

## Visualization
- CI: Adding --verbose flag to colcon test-result. (2348467)

---

# OAP5 Changelog (2020-01-15)
Based on autoware.ai version 1.13.0

## Common
- Merge in upstream autoware version 1.13.0 (f4657ba)
- CI: Dropping Kinetic builds. (a6146e3)
- Vector map loader fix and improvement (6016984)
- Fixing bug where gnss would always round down lat & lon (a6518cf)
- Merge in upstream autoware (a0e2e31)

## Core Perception
- Only run tests that require CUDA when a valid NVidia graphics card is found. (cf4a98b)
- Merge in upstream autoware version 1.13.0 (59e9a88)
- Feature Projection Vector Map Loading Improvement (35a5e9f)
- Add configurable parameter for ndt_matching output tf pose name. (ab580f4)
- Add feature projection launch configurations (e60941f)
- CI: Dropping Kinetic builds. (599cbc0)
- Fix launch file for vision_darknet_detect (8cbb636)
- Add rgb8 encoding support (be0cf28)
- Fixing unit test values due to MR4 in common subgroup (665bfc6)
- Merge in upstream autoware (44e4ae6)

## Core Planning
- Fix pure_pursuit command velocity (c070806)
- Fix avoidSuddenDeceleration function (4b2f0a8)
- Temporary fix to add use_lgsim param for the twist_gate issue on LG Sim (dedd901)
- Refactor the code to publish /base_waypoints (e70c5ce)
- Stanley controller implementation (4c4b10a)
- Disable reset of is_waypoint_set flag after each control loop (49fc20e)
- Fix thread synchronization issue inside astar_avoid (17184d6)
- Fix lane_planner indexing bug (0fa059b)
- Remove extra waypoint which is prepended to the published final_waypoints (f91c6b7)
- Merge in upstream autoware version 1.13.0 (8270aae)
- Fix curvature calculation for pure_pursuit (b93d640)
- Decrease Decision Maker map load time (80741c1)
- Feature/turn signals (28a49e4)
- CI: Dropping Kinetic builds. (f8e6cb2)
- Fix decision_maker launch bug (6bd2620)
- Merge in upstream autoware (213f366)

## Docker
- Merge in upstream autoware version 1.13.0 (f74d4aca)
- CI: Dropping Kinetic builds. (33092947)
- Merge in upstream autoware (bb183b9b)

## Drivers
- Change output to debug (dbccb32)
- Merge in upstream autoware version 1.13.0 (fedbd20)
- Use default ssc topic namespace (f2659db)
- CI: Dropping Kinetic builds. (f6de1e6)
- Removing dependency on pacmod_msgs which is no longer necessary. (0547ed1)
- Merge in upstream autoware (9062686)
- Update ssc_interface to be platform independent (f529c8b)

## Messages
- Merge in upstream autoware version 1.13.0 (26d46e8)
- CI: Dropping Kinetic builds. (3596852)
- Merge in upstream autoware (bb44697)

## Simulation
- Merge in upstream autoware version 1.13.0 (e3b6441)
- CI: Dropping Kinetic builds. (af602fd)
- Merge in upstream autoware (b8b62c9)

## Utilities
- Merge in upstream autoware version 1.13.0 (d755053)
- CI: Dropping Kinetic builds. (9ca83ec)
- Merge in upstream autoware (bb1ad9a)

## Visualization
- Merge in upstream autoware version 1.13.0 (10955e0)
- CI: Dropping Kinetic builds. (159a820)
- Merge in upstream autoware (f6bf051)

---

# OAP4 Changelog (2019-10-02)
Based on autoware.ai version 1.12.0

## Common
- WPF: Replace check for running node with launch param. (d17e661a)
- CI: Using AS-generated Docker containers. (785d6176)
- Upstream Merge 20170727 (f69e701a)
- Updating CI to only build for as/master. (5ad52d79)
- Updating CI to point to AS repos. (0c704cef)
- Updating build_depends.repos with AS repos. (a02a1cd0)

## Core Perception
- Feature/tld tensorflow (5810c79e)
- Fixing bug where nmea2tfpose would always assume positive coordinates. (55d7125b)
- Clean-up/Lint Traffic Light Recognizer (TLR) (e2fb5f67)
- Add Dead Reckoner Package (a82994d7)
- Merge branch 'feature/OAP-114_parameter_ndt_match' into 'as/master' (66ab41b4)
- fitness score made configurable through launch files (d794bc4e)
- Tracker: Unused param cleanup (4e0318a8)
- Adding gpsins_localizer from as/oap. (75970c26)
- CI: Using AS-generated Docker containers. (ce957bcf)
- Upstream Merge 20190727 (aca908dc)
- Updating build_depends.repos and CI to point to AS repos. (d246fa97)
- Fixes location of remap in naive_motion_predict launch file. (bbdf51a5)
- CI: Removing unused variables and combining common scripts. (f7053bdc)
- Adding startup params to some nodes. (bd9bd795)
- Updating all package.xml files to format 2 (REP-140). (42fc9c9d)
- Remove dead code and add note to README in euclidean_cluster (38073ada, 0f8be75c)

## Core Planning
- Fix/waypoint replanner w decision maker (aa1ee7e9)
- CI: Using AS-generated Docker containers. (7e09f03f)
- Upstream Merge 20190727 (035251a2)
- Updating build_depends.repos and CI to use AS repos. (ce5c5a2e)

## Docker
- Upstream Merge 20190731 (13592c70)
- Replaces all Autoware refs with AS. (f09b7836)

## Drivers
- SSC Interface: Change VelocityAccel to VelocityAccelCov. (8795c34c)
- AS Interface: Reduced default decel limit to 3.0. (f1cfa92f)
- CI: Using AS-generated Docker containers. (3b0d839f)
- Upstream Merge 20190727 (3c69d541)
- Updating build_depends.repos and CI to use AS repos. (72ae9c98)

## Messages
- Adding service message for TLR. (ffb3b3f8)
- As/ci use as containers (71b8aeb2)
- Upstream Merge 20190727 (f6f93d64)
- Updating CI to use AS repos. (f0df3d48)

## Simulation
- CI: Using AS-generated Docker containers. (8f2c504e)

## Utilities
- CI: Using AS-generated Docker containers. (acc36af1)
- Upstream Merge 20190727 (d0ff39b3)
- Updating build_depends.repos and CI to use AS repos. (bcdb6a65)

## Visualization
- CI: Using AS-generated Docker containers. (acc36af1)
- Upstream Merge 20190727 (d0ff39b3)
- Updating build_depends.repos and CI to use AS repos. (bcdb6a65)
